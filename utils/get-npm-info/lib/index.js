"use strict";

import axios from "axios";
import semver from "semver";

function getNpmInfo(name, registry) {
  if (!name) {
    return;
  }
  const defaultRegistry = getDefaultRegistry();
  const registryUrl = registry || defaultRegistry;
  const npmUrl = `${registryUrl}/${name}`;
  return axios
    .get(npmUrl)
    .then((res) => {
      if (res.status === 200) {
        return res.data;
      } else {
        Promise.reject(new Error("get npm info err"));
      }
    })
    .catch((err) => {
      Promise.reject(err);
    });
}

export function getDefaultRegistry(isLocal = true) {
  return isLocal ? "https://registry.npmmirror.com" : "https://registry.npmjs.org";
}

export async function getLastVersion(name, registry) {
  try {
    const data = await getNpmInfo(name, registry);
    const versions = Object.keys(data.versions);
    versions.sort((a, b) => (semver.gt(b, a) ? 1 : -1));
    return versions[0];
  } catch (e) {
    throw e;
  }
}
