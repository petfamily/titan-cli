"use strict";
import { readFileSync } from "fs";
import path from "path";

/**
 * 判断是否是对象
 * @param {*} obj
 * @returns
 */
export function isObject(obj) {
  return Object.prototype.toString.call(obj) === "[object Object]";
}

/**
 * 读取.json后缀的文件
 * @param {*} file  文件名称
 * @param {*} url 路径
 * @returns
 */
export function readJsonFile(file, url) {
  return JSON.parse(readFileSync(new URL(file, url)));
}

export function fromatPath(path) {
  if (typeof path !== "string") {
    throw Error("the path mast be a string");
  }
  return path.sep === "/" ? path : path.replace(/\\/g, "/");
}
