"use strict";

import log from "npmlog";

log.level = process.env.LOG_LEVEL || "info";

log.heading = process.env.CLI_NAME || "cli";

log.addLevel("success", 2000, { fg: "blue", bold: true });

export default log;
