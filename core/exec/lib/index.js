import path from "node:path";
import { spawn } from "node:child_process";
import colors from "colors";
import ora from "ora";
import Package from "@leisurely/package";
import log from "@leisurely/log";

const DICT = {
  init: "@leisurely/init",
  add: "@leisurely/add",
  publish: "@leisurely/publish",
};
const CACHE_DIR = "dependencies";

export default async function exec() {
  let targetPath = process.env.CLI_TARGET_PATH;
  let storeDir = "";
  const homePath = process.env.CLI_HOME_PATH;

  log.verbose("local targetPath: ", targetPath);
  log.verbose("homePath: ", homePath);

  const cmdObj = arguments[arguments.length - 1];
  const cmdName = cmdObj.name();
  const pkgName = DICT[cmdName];
  const pkgVersion = "latest";
  let pkg = null;
  if (!targetPath) {
    // 运行远程包
    targetPath = path.resolve(homePath, CACHE_DIR);
    storeDir = path.resolve(targetPath, "node_modules");
    log.verbose("targetPath: ", targetPath);
    log.verbose("storeDir: ", storeDir);
    pkg = new Package({ targetPath, storeDir, name: pkgName, version: pkgVersion });
    const spinner = ora("prepare exec package...").start();
    if (await pkg.exists()) {
      // 更新
      try {
        await pkg.update();
        spinner.succeed("exec package is ready");
      } catch (e) {
        spinner.fail("load exec package failed");
      }
    } else {
      // 安装
      try {
        await pkg.install();
        spinner.succeed("exec package is ready");
      } catch (e) {
        spinner.fail("load exec package failed");
      }
    }
  } else {
    // 本地
    pkg = new Package({ targetPath, storeDir, name: pkgName, version: pkgVersion });
  }

  const entryPath = await pkg.getEntryPath();

  if (!entryPath) {
    throw Error(colors.red("entry path get failed"));
  }

  try {
    log.verbose(entryPath, "entryPath");
    // 去掉继承参数、私有参数、 无用参数
    const args = Array.from(arguments);
    const cmd = args[args.length - 1];
    const obj = Object.create(null);
    Object.keys(cmd).forEach((key) => {
      if (cmd.hasOwnProperty(key) && !key.startsWith("_") && key !== "parent") {
        obj[key] = cmd[key];
      }
    });
    args[args.length - 1] = obj;

    const code = `import('file:///${entryPath}').then(module => module.default.call(null, ${JSON.stringify(args)}))`;

    const child = spawn("node", ["-e", code], {
      cwd: process.cwd(),
      stdio: "inherit",
    });

    child.on("error", (e) => {
      log.error(e.message);
      process.exit(1);
    });

    child.on("exit", (e) => {
      log.verbose("exec success");
      process.exit(e);
    });
  } catch (e) {
    log.error(e.message);
  }
}
