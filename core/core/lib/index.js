import path from "node:path";
import { homedir } from "node:os";
import semver from "semver";
import colors from "colors";
import rootCheck from "root-check";
import dotenv from "dotenv";
import { pathExistsSync } from "path-exists";
import { Command } from "commander";
import log from "@leisurely/log";
import exec from "@leisurely/exec";
import { readJsonFile } from "@leisurely/utils";
import { getLastVersion } from "@leisurely/get-npm-info";

const userHome = homedir();
const pkg = readJsonFile("../package.json", import.meta.url);
const cliName = getCliName(pkg);
// 设置日志头
log.heading = cliName;
// 默认的缓存目录
const defaultCliHome = `.${cliName}`;

async function core() {
  try {
    prepare();
    registerCommand();
  } catch (e) {
    if (process.env.LOG_LEVEL === "verbose") {
      log.error(e);
    } else {
      log.error(e);
    }
  }
}

/**
 * 注册命令
 */
function registerCommand() {
  const program = new Command();
  program
    .name(cliName)
    .description(pkg.description)
    .usage("<command> [options]")
    .version(pkg.version, "-v, --version", "output the current version")
    .option("-d, --debug", "output extra debugging")
    .option("-tp, --targetPath <targetPath>", "specify a local file to run");

  program
    .command("init [projectName]")
    .description("init a project")
    .option("-f, --force", "whether to force init")
    .action(exec);

  program.on("option:debug", function () {
    process.env.LOG_LEVEL = "verbose";
    log.level = "verbose";
    log.info("The debug mode has been enabled");
  });

  // 监听targetPath
  program.on("option:targetPath", function (value) {
    process.env.CLI_TARGET_PATH = value;
  });

  // 监听未知命令
  program.on("command:*", function (cmd) {
    log.error(`unknow command ${cmd}`);
    const availableCommands = program.commands.map((cmd) => cmd.name());
    log.info("usable command", availableCommands);
  });

  program.parse(process.argv);

  // 获取参数
  const options = program.opts();

  // 什么都不输入 默认打印帮助文档
  if (program.args?.length < 1) {
    program.outputHelp();
    console.log();
  }
}

/**
 * 准备阶段
 */
async function prepare() {
  // 保存脚手架名称到env
  process.env.CLI_NAME = cliName;

  // 打印版本号
  log.info("cli-version", pkg.version);

  // 检查是否是root账户
  rootCheck();

  // 检查是否有用户目录
  const isExists = pathExistsSync(userHome);
  if (!isExists) {
    throw Error(colors.red("the user home directory not exist"));
  }

  // 检查是否存在.env, 没有则创建
  const dotenvPath = path.resolve(userHome, ".env");
  if (!pathExistsSync(dotenvPath)) {
    dotenv.config({ path: dotenvPath });
  }

  // 设置cli主目录，用于缓存包
  const cliHomePath = process.env.CLI_HOME
    ? path.join(userHome, process.env.CLI_HOME)
    : path.join(userHome, defaultCliHome);

  process.env.CLI_HOME_PATH = cliHomePath;

  // 检查更新
  checkUpdate();
}

/**
 * 检查更新
 */
async function checkUpdate() {
  const { name, version: currentVersion } = pkg;
  const latestVersion = await getLastVersion(name);
  if (latestVersion && semver.gt(latestVersion, currentVersion)) {
    console.log();
    console.log();
    log.warn(
      colors.yellow(`please update ${cliName}, current version: ${currentVersion}, online version: ${latestVersion}`)
    );
    log.info(`update command: npm install -g ${name}`);
    console.log();
  }
}

// 获取脚手架名称： 脚手架名称 != 包名
function getCliName(pkg) {
  return Object.keys(pkg.bin)[0];
}

export default core;
