import fs from "node:fs";
import path from "node:path";
import { spawn } from "node:child_process";
import inquirer from "inquirer";
import fsExtra from "fs-extra";
import semver from "semver";
import ora from "ora";
import glob from "glob";
import ejs from "ejs";
import log from "@leisurely/log";
import Package from "@leisurely/package";
import Command from "@leisurely/command";

const NAME_REG = /^[a-zA-Z]+([-][a-zA-Z][a-zA-Z0-9]*|[_][a-zA-Z][a-zA-Z0-9]*|[a-zA-Z0-9])*$/;
// 项目模板
const PROJECT_TEMPLATE = [
  { value: "@leisurely/vue-tp", name: "vue-test" },
  { value: "@/leisurely/react-quick", name: "react" },
  { value: "@/leisurely/react-ts-quick", name: "react-ts" },
  { value: "@/leisurely/vue-ts-quick", name: "vue-ts" },
  { value: "@/leisurely/vue-quick", name: "vue" },
  { value: "@/leisurely/h5-quick", name: "h5" },
  { value: "@/leisurely/document-quick", name: "document" },
  { value: "@leisurely/component-react-quick", name: "component-react" },
  { value: "@/leisurely/component-vue-quick", name: "component-vue" },
];

class InitCommand extends Command {
  init() {
    const [projectName, options = {}] = this._argv;
    if (projectName && !NAME_REG.test(projectName)) {
      throw Error("The project name is invalid. Replace it");
    }

    if (projectName) {
      // 默认项目名称
      this.projectName = projectName;
      // 项目根目录
      this.projectRoot = path.resolve(process.cwd(), projectName);
      // 判断路径是否存在，不存在则创建
      fsExtra.ensureDirSync(this.projectRoot);
    } else {
      const wrokPath = process.cwd();
      const pathArr = wrokPath.split(path.sep);
      this.projectName = pathArr[pathArr.length - 1];
      this.projectRoot = process.cwd();
    }

    // 是否强制生成
    this.force = options.force || false;

    log.verbose(`project root: ${this.projectRoot}`);
  }

  async exec() {
    try {
      const projectInfo = await this.prepare();
      if (projectInfo) {
        this.projectInfo = projectInfo;
        // 1. 下载
        await this.downloadTemplate();
        // 2. 安装模板
        await this.installTemplate();
        // 3. 安装依赖
        this.execCommand();
      }
    } catch (e) {
      log.error(e.message);
    }
  }

  execCommand() {
    log.info(`${process.env.NPM_CLIENT} install --registry=${process.env.NPM_REGISTRY}`);
    log.info(`project root: ${this.projectRoot}`);

    const startTime = new Date().getTime();
    const child = spawn("npm", ["install", `--registry=${process.env.NPM_REGISTRY}`], {
      cwd: this.projectRoot,
      stdio: "inherit",
    });
    child.on("error", (e) => {
      log.error(e.message);
    });

    child.on("exit", (e) => {
      log.info("init success");
      const endTime = new Date().getTime();
      const diffTime = (endTime - startTime) / 1000;
      log.info(`Done in ${diffTime.toFixed(2)}s`);
    });
  }

  async installTemplate() {
    const templatePath = path.resolve(this.templatePkg.cacheFilePath, "template");
    const spinner = ora(`copyfile to ${this.projectRoot}...`).start();
    try {
      fsExtra.ensureDirSync(templatePath);
      fsExtra.copySync(templatePath, this.projectRoot);
    } catch (e) {
      spinner.fail("copy failed");
      throw e;
    }
    spinner.text = "render...";
    // 将输入的信息渲染到下载的模板里
    const ignoreList = ["**/node_modules/**", "public/**"];
    try {
      await this.render({ ignore: ignoreList });
      spinner.succeed("render success");
    } catch (e) {
      spinner.fail("render failed");
    }
  }

  // 下载模板
  async downloadTemplate() {
    const { template } = this.projectInfo;
    const templateInfo = PROJECT_TEMPLATE.find((item) => item.value === template);
    const targetPath = path.resolve(process.env.CLI_HOME_PATH, "template");
    const storeDir = path.resolve(targetPath, "node_modules");
    const { value } = templateInfo;
    const pkg = new Package({
      targetPath,
      storeDir,
      name: value,
      version: "latest",
    });

    log.verbose(`package info: ${JSON.stringify(pkg)}`);

    if (!(await pkg.exists())) {
      const spinner = ora("downloading").start();
      try {
        await pkg.install();
      } catch (e) {
        spinner.fail("download failed!");
        throw Error(e);
      }
      if (await pkg.exists()) {
        spinner.succeed("download success!");
      }
    }
    this.templatePkg = pkg;
  }

  async render({ ignore = "" }) {
    const currentDir = process.cwd();
    const projectInfo = this.projectInfo;
    return new Promise((resolve, reject) => {
      glob("**", { cwd: currentDir, ignore, nodir: true }, (err, files) => {
        if (err) {
          reject(err);
        }
        Promise.all(
          files.map(async (file) => {
            const filePath = path.join(currentDir, file);
            try {
              const result = await ejs.renderFile(filePath, projectInfo, {});
              fsExtra.writeFileSync(filePath, result);
              return result;
            } catch (err) {
              throw Error(err);
            }
          })
        )
          .then(() => {
            resolve();
          })
          .catch((e) => {
            reject(e);
          });
      });
    });
  }

  async prepare() {
    const isEmpty = this.isDirEmpty(this.projectRoot);
    if (!isEmpty) {
      const { isContinue } = await inquirer.prompt([
        {
          type: "confirm",
          name: "isContinue",
          message: this.force
            ? "Are you sure to clear the current directory and continue?"
            : "The current folder is not empty. Would you like to clear it and continue?",
        },
      ]);
      if (isContinue) {
        fsExtra.emptyDirSync(this.projectRoot);
      } else {
        return false;
      }
    }

    return await this.getProjectInfo();
  }

  async getProjectInfo() {
    const promptList = [
      {
        type: "input",
        name: "name",
        message: `please input project name:`,
        default: this.projectName,
        validate(input) {
          const done = this.async();
          setTimeout(function () {
            if (!NAME_REG.test(input)) {
              done("The name is invalid. Replace it");
              return;
            }
            done(null, true);
          }, 0);
        },
      },
      {
        type: "input",
        name: "version",
        message: `please input project version:`,
        default: "1.0.0",
        validate(input) {
          const done = this.async();
          setTimeout(function () {
            if (!semver.valid(input)) {
              done("The version is invalid. Replace it");
              return;
            }
            done(null, true);
          }, 0);
        },
        filter(input) {
          const value = semver.valid(input);
          return value ? value : input;
        },
      },
      {
        type: "input",
        name: "description",
        message: "please input component descriptions:",
      },
      {
        type: "list",
        name: "template",
        message: `please choice template`,
        choices: PROJECT_TEMPLATE,
      },
      {
        type: "list",
        name: "npmClient",
        message: `please choice npm client`,
        choices: [
          { name: "npm", value: "npm" },
          { name: "cnpm", value: "cnpm" },
          { name: "tnpm", value: "tnpm" },
          { name: "yarn", value: "yarn" },
          { name: "pnpm", value: "pnpm" },
        ],
        initial: 4,
      },
      {
        type: "list",
        name: "npmRegistry",
        message: `please choice npm registry`,
        choices: [
          { name: "taobao", value: "https://registry.npmmirror.com" },
          { name: "npm", value: "https://registry.npmjs.org/" },
        ],
      },
    ];

    const answer = await inquirer.prompt(promptList);
    const projectInfo = {
      ...answer,
    };

    process.env.NPM_CLIENT = answer.npmClient;
    process.env.NPM_REGISTRY = answer.npmRegistry;

    log.verbose(`input info：${JSON.stringify(projectInfo)}`);
    return projectInfo;
  }

  async initGit(projectRoot) {
    const isGit = fs.existsSync(path.join(projectRoot, ".git"));
    if (isGit) return;
    try {
      spawn("git", ["init"], { cwd: projectRoot, stdio: "inherit" });
      log.info(`Git initialized success`);
    } catch {
      log.error(`Initial the git repo failed`);
    }
  }

  // 判断目录是否为空
  isDirEmpty(dir) {
    return fs.readdirSync(dir).length === 0;
  }
}

function init(argv) {
  return new InitCommand(argv);
}

export default init;
