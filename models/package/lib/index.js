"use strict";
import path from "path";
import { pathExists } from "path-exists";
import { readFileSync } from "node:fs";
import { fromatPath, isObject } from "@leisurely/utils";
import npminstall from "npminstall";
import { getDefaultRegistry, getLastVersion } from "@leisurely/get-npm-info";

export default class Package {
  constructor(options) {
    if (!isObject(options)) {
      throw Error("options mast be an Object");
    }
    const { targetPath, storeDir, name, version } = options;
    this.targetPath = targetPath;
    this.storeDir = storeDir;
    this.name = name;
    this.version = version;
    this.cacheFilePathPrefix = this.name.replace("/", "_");
  }

  get cacheFilePath() {
    return path.resolve(this.storeDir, `_${this.cacheFilePathPrefix}@${this.version}@${this.name}`);
  }

  // 判断包是否存在
  async exists() {
    // 远程包
    if (this.storeDir) {
      // 最新版本需要拿到具体的版本号
      if (this.version === "" || this.version === "latest") {
        this.version = await getLastVersion(this.name);
      }
      return await pathExists(this.cacheFilePath);
    } else {
      return await pathExists(this.targetPath);
    }
  }

  // 安装包
  async install() {
    await npminstall({
      root: this.targetPath,
      storeDir: this.storeDir,
      registry: process.env.NPM_REGISTRY || getDefaultRegistry(),
      pkgs: [{ name: this.name, version: this.version }],
    });
  }

  // 更新包
  async update() {
    const latestVersion = await getLastVersion(this.name);
    if (this.version === latestVersion) {
      return;
    }

    await npminstall({
      root: this.targetPath,
      storeDir: this.storeDir,
      registry: process.env.NPM_REGISTRY || getDefaultRegistry(),
      pkgs: [{ name: this.name, version: latestVersion }],
    });
    this.version = latestVersion;
  }

  // 获取入口文件路径
  async getEntryPath() {
    const dir = this.storeDir ? this.cacheFilePath : this.targetPath;
    if (!dir) {
      return null;
    }
    const baseUrl = path.resolve(dir, "package.json");
    const pkgFile = JSON.parse(readFileSync(baseUrl));
    if (pkgFile?.main) {
      return fromatPath(path.resolve(dir, pkgFile.main));
    } else {
      return null;
    }
  }
}
