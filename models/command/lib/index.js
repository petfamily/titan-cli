import semver from "semver";
import colors from "colors";
import log from "@leisurely/log";

const LOWEST_NODE_VERSION = "14.0.0";

class Command {
  constructor(argv) {
    if (!argv) {
      throw new Error("The argument cannot be empty!");
    }

    if (!Array.isArray(argv) || argv.length < 1) {
      throw new Error("The argument must be an array and not empty!");
    }

    this._argv = argv;
    const runner = new Promise((resolve, reject) => {
      let chain = Promise.resolve();
      chain = chain.then(() => this.checkNodeVersion());
      chain = chain.then(() => this.initArgs()); // 参数校验
      chain = chain.then(() => this.init()); // 执行用户自定义逻辑
      chain = chain.then(() => this.exec()); // 执行用户自定义逻辑
      chain.catch((err) => {
        log.error(err.message);
      });
    });
  }

  initArgs() {
    const argv = this._argv;
    const lastIndex = argv.length - 1;
    this._cmd = argv[lastIndex];
    this._argv = argv.slice(0, lastIndex);
  }

  checkNodeVersion() {
    if (!semver.gte(process.version, LOWEST_NODE_VERSION)) {
      throw Error(colors.red(`The node version need gte v${LOWEST_NODE_VERSION} for ${process.env.CLI_NAME}`));
    }
  }

  init() {
    throw new Error("init is must");
  }

  exec() {
    throw new Error("exec is must");
  }
}

export default Command;
